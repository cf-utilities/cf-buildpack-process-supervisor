# The Process Supervisor ("PS") Buildpack

## Summary

This buildpack instantiates and regulates applications and services defined
within said applications. It can handle both oblivious applications and ones
which know of its existence. Process Supervisor uses [runit](http://smarden.org/runit/) to execute and
manage services. It is capable of starting apps as the only buildpack but only
under certain circumstances (outlined below).  Otherwise Process Supervisor
relies on [multi-buildpack](https://bitbucket.org/cf-utilities/cf-buildpack-multi) to line up other buildpacks for it.

## Usage

If the application can be started without needing a language-specific buildpack
then it can be started with Process Supervisor as its buildpack listed in
`manifest.yml` or with the `-b` option of `cf push`.

For all other cases, extract the current buildpack URL from the application's
`manifest.yml` or `cf push -b <buildpack>` invocation.  Construct a
`.buildpacks` file in the application's repository's root directory which looks
like this:

    <current buildpack URL>
    https://bitbucket.com/cf-utilities/cf-buildpack-process-supervisor

Next, modify the manifest or `cf push` invocation to reference the Multi
buildpack:

    https://bitbucket.com/cf-utilities/cf-buildpack-multi

NB At present the Multi buildpack only works with external buildpacks, not
those built in to the target Cloud Foundry. This limitation may be addressed in
the future.

Finally, `cf push` the application. During the staging process, you should
notice lines roughly matching these scroll by, after the application's usual
buildpack/runtime has finished:

    =====> Downloading Buildpack: https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor
    =====> Detected Framework: Process Supervisor
    Using release configuration from last framework (Process Supervisor).

### Configuration

#### Adding Services

Process Supervisor looks for services in
`platform/cloudfoundry/services/in-container/*` where it should find one
directory per service with names related to the service. Each service directory
should contain at least two scripts: `run` and `finish`. Process Supervisor
will execute `run` and expect it not to finish.  If it does finish Process
Supervisor will exectute `finish` then loop back to executing `run` again. Any
additional services defined in the app in the above structure will be executed
and managed
by Process Supervisor.

#### Disabling Services

Process Supervisor currently has one configurable environment variable which is
used for disabling services. Environment variables can be set by an
application's `manifest.yml` or `cf set-env` invocations.

`PS_DISABLE_SERVICE_<NAME>`:  In order to disable a service located in
`platform/cloudfoundry/services/in-container/the-service.name` the environment
variable `PS_DISABLE_SERVICE_THE_SERVICE_NAME` should be set.

### Testing

In order to reduce the size of this buildpack on disk, tests can be found in
[another repo](https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor-tests).



